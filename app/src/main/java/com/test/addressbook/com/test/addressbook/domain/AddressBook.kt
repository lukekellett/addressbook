package com.test.addressbook.com.test.addressbook.domain

import com.test.addressbook.com.test.addressbook.domain.model.Contact

interface AddressBook {

    val id: Int

    fun addContact(name: String, phoneNumber: String): Contact
    fun removeContact(contact: Contact): Boolean
    fun printContacts(): String
    fun contactCount(): Int
    fun getAllContacts(): Collection<Contact>

}