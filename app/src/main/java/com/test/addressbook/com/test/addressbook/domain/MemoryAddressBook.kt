package com.test.addressbook.com.test.addressbook.domain

import com.test.addressbook.com.test.addressbook.domain.model.Contact
import com.test.addressbook.com.test.addressbook.domain.util.IdentityUtil


class MemoryAddressBook : AddressBook {

    override val id: Int = IdentityUtil.getNextId()
    private var store: HashMap<Int, Contact> = hashMapOf()

    override fun addContact(name: String, phoneNumber: String): Contact {
        var newContact = Contact(IdentityUtil.getNextId(), name, phoneNumber)
        var existingContact: Contact? = null
        store.values.forEach {
            if(it.equals(newContact)) {
                existingContact = it
                true
            }
        }
        if(existingContact != null) {
            return existingContact!!
        } else {
            store.put(newContact.id, newContact)
            return newContact
        }
    }

    override fun removeContact(contact: Contact): Boolean {
        return store.remove(contact.id) != null
    }

    override fun printContacts(): String {
        var stringBuilder = StringBuilder()
        store.entries.forEach {
            stringBuilder.append("${it.value}\n")
        }

        return stringBuilder.toString()

    }

    override fun contactCount(): Int {
        return store.size
    }

    override fun getAllContacts(): Collection<Contact> {
        return store.values
    }

}