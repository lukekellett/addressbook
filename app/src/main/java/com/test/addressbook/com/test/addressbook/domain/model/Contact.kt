package com.test.addressbook.com.test.addressbook.domain.model


class Contact(val id: Int, val name: String, val phoneNumber: String) {

    override fun equals(other: Any?): Boolean {
        if(other == null) {
            return false
        }

        if(other is Contact) {
            return id == other.id || phoneNumber == other.phoneNumber
        }

        return false

    }

    override fun toString(): String {
        return "id: $id\tname: $name\tphoneNumber: $phoneNumber"
    }

}