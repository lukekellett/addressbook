package com.test.addressbook.com.test.addressbook.domain.util

object IdentityUtil {
    private var _nextId = 0

    fun getNextId(): Int {
        _nextId++
        return _nextId
    }

    fun reset() {
        _nextId = 0
    }
}