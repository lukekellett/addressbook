package com.test.addressbook.com.test.addressbook.domain.dagger

import com.test.addressbook.com.test.addressbook.domain.AddressBook
import com.test.addressbook.com.test.addressbook.domain.MemoryAddressBook
import dagger.Module
import dagger.Provides

@Module
class DomainModule {

    @Provides
    fun addressBook(): AddressBook {
        return MemoryAddressBook()
    }

}