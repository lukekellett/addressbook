package com.test.addressbook.com.test.addressbook.domain.dagger

import com.test.addressbook.com.test.addressbook.domain.AddressBookCollection
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(DomainModule::class))
interface DomainComponent {
    fun inject(AddressBookFactory: AddressBookCollection.AddressBookFactory)
}