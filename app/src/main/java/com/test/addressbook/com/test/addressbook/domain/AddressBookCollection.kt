package com.test.addressbook.com.test.addressbook.domain

import com.test.addressbook.com.test.addressbook.domain.dagger.DaggerDomainComponent
import com.test.addressbook.com.test.addressbook.domain.dagger.DomainModule
import com.test.addressbook.com.test.addressbook.domain.model.Contact
import javax.inject.Inject


class AddressBookCollection {

    private var store: HashMap<Int, AddressBook> = hashMapOf()

    class AddressBookFactory {
        @Inject
        lateinit var addressBook: AddressBook

        init {
            domainComponent.inject(this)
        }

        companion object {
            val domainComponent = DaggerDomainComponent
                    .builder()
                    .domainModule(DomainModule())
                    .build()
        }

    }

    fun addAddressBook(): AddressBook {
        var addressBook = AddressBookFactory().addressBook
        store.put(addressBook.id, addressBook)
        return addressBook
    }

    fun removeAddressBook(addressBook: AddressBook): Boolean {
        return store.remove(addressBook.id) != null
    }

    fun printContacts(uniqueOnly: Boolean): String {
        var stringBuilder = StringBuilder()
        if(uniqueOnly) {
            var contacts = mutableListOf<Contact>()
            store.values.forEach {
                it.getAllContacts().forEach {
                    if(!contacts.contains(it)) {
                        contacts.add(it)
                        stringBuilder.append("${it}\n")
                    }
                }
            }
        } else {
            store.values.forEach {
                it.getAllContacts().forEach {
                    stringBuilder.append("${it}\n")
                }
            }
        }
        return stringBuilder.toString()
    }

    fun addressBookCount(): Int {
        return store.size
    }

}