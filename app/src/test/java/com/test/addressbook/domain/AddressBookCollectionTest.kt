package com.test.addressbook.domain

import com.test.addressbook.com.test.addressbook.domain.MemoryAddressBook
import com.test.addressbook.com.test.addressbook.domain.AddressBookCollection
import com.test.addressbook.com.test.addressbook.domain.model.Contact
import com.test.addressbook.com.test.addressbook.domain.util.IdentityUtil
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class AddressBookCollectionTest {

    @Before
    fun setup() {
        IdentityUtil.reset()
    }

    @Test
    fun removeAddressBook_addressBookRemoved() {
        var addressBookCollection = AddressBookCollection()
        var addressBook = addressBookCollection.addAddressBook()

        assertEquals(1, addressBookCollection.addressBookCount())

        addressBookCollection.removeAddressBook(addressBook)
        assertEquals(0, addressBook.contactCount())

    }

    @Test
    fun printContacts_uniqueOnly_contactsPrinted() {
        var addressBookCollection = mockAddressBookCollection()

        var expectedContacts = "id: 1\tname: Name 1\tphoneNumber: 12345\n" +
                "id: 2\tname: Name 2\tphoneNumber: 123456\n" +
                "id: 3\tname: Name 3\tphoneNumber: 1234567\n"

        assertEquals(expectedContacts, addressBookCollection.printContacts(true))

    }

    @Test
    fun printContacts_allContacts_contactsPrinted() {
        var addressBookCollection = mockAddressBookCollection()

        var expectedContacts = "id: 1\tname: Name 1\tphoneNumber: 12345\n" +
                "id: 2\tname: Name 2\tphoneNumber: 123456\n" +
                "id: 3\tname: Name 3\tphoneNumber: 1234567\n" +
                "id: 1\tname: Name 1\tphoneNumber: 12345\n" +
                "id: 2\tname: Name 2\tphoneNumber: 123456\n" +
                "id: 3\tname: Name 3\tphoneNumber: 1234567\n" +
                "id: 1\tname: Name 1\tphoneNumber: 12345\n" +
                "id: 2\tname: Name 2\tphoneNumber: 123456\n" +
                "id: 3\tname: Name 3\tphoneNumber: 1234567\n"

        assertEquals(expectedContacts, addressBookCollection.printContacts(false))

    }

    private fun mockAddressBookCollection(): AddressBookCollection {

        var addressBookCollection = AddressBookCollection()
        var addressBook1 = addressBookCollection.addAddressBook()
        var addressBook2 = addressBookCollection.addAddressBook()
        var addressBook3 = addressBookCollection.addAddressBook()

        IdentityUtil.reset()
        addressBook1.addContact("Name 1", "12345")
        addressBook1.addContact("Name 2", "123456")
        addressBook1.addContact("Name 3", "1234567")
        IdentityUtil.reset()
        addressBook2.addContact("Name 1", "12345")
        addressBook2.addContact("Name 2", "123456")
        addressBook2.addContact("Name 3", "1234567")
        IdentityUtil.reset()
        addressBook3.addContact("Name 1", "12345")
        addressBook3.addContact("Name 2", "123456")
        addressBook3.addContact("Name 3", "1234567")

        return addressBookCollection

    }

}