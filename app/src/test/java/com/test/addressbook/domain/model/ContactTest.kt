package com.test.addressbook.domain.model

import com.test.addressbook.com.test.addressbook.domain.model.Contact
import com.test.addressbook.com.test.addressbook.domain.util.IdentityUtil
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ContactTest {

    @Before
    fun setup() {
        IdentityUtil.reset()
    }

    @Test
    fun equals_sameId_returnsTrue() {
        var contact1 = Contact(IdentityUtil.getNextId(),"Name 1", "1")
        IdentityUtil.reset()
        var contact2 = Contact(IdentityUtil.getNextId(),"Name 2", "2")
        assertTrue(contact1.equals(contact2))
    }

    @Test
    fun equals_samePhoneNumber_returnsTrue() {
        var contact1 = Contact(IdentityUtil.getNextId(),"Name 1", "1")
        var contact2 = Contact(IdentityUtil.getNextId(),"Name 2", "1")
        assertTrue(contact1.equals(contact2))
    }

    @Test
    fun equals_differentIdAndPhoneNumber_returnsFalse() {
        var contact1 = Contact(IdentityUtil.getNextId(),"Name 1", "1")
        var contact2 = Contact(IdentityUtil.getNextId(),"Name 2", "2")
        assertFalse(contact1.equals(contact2))
    }

}