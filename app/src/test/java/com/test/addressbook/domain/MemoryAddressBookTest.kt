package com.test.addressbook.domain

import com.test.addressbook.com.test.addressbook.domain.MemoryAddressBook
import com.test.addressbook.com.test.addressbook.domain.model.Contact
import com.test.addressbook.com.test.addressbook.domain.util.IdentityUtil
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MemoryAddressBookTest {

    @Before
    fun setup() {
        IdentityUtil.reset()
    }

    @Test
    fun new_idsIncrementSequentially() {
        assertEquals(1, MemoryAddressBook().id)
        assertEquals(2, MemoryAddressBook().id)
        IdentityUtil.reset()
        assertEquals(1, MemoryAddressBook().id)
        assertEquals(2, MemoryAddressBook().id)

    }

    @Test
    fun addContact_contactAddedOnlyOnce() {
        var addressBook = MemoryAddressBook()

        assertEquals(0, addressBook.contactCount())

        addressBook.addContact("Jane", "12345")
        assertEquals(1, addressBook.contactCount())

        addressBook.addContact("Jane Doe", "12345")
        assertEquals(1, addressBook.contactCount())

    }

    @Test
    fun removeContact_contactRemoved() {
        var addressBook = MemoryAddressBook()

        var contact = addressBook.addContact("Jane", "12345")
        assertEquals(1, addressBook.contactCount())

        addressBook.removeContact(contact)
        assertEquals(0, addressBook.contactCount())

    }

    @Test
    fun printContacts_contactsPrinted() {

        var addressBook = MemoryAddressBook()
        IdentityUtil.reset()

        addressBook.addContact("Name 1", "12345")
        addressBook.addContact("Name 2", "123456")
        addressBook.addContact("Name 3", "1234567")

        var expectedContacts = "id: 1\tname: Name 1\tphoneNumber: 12345\n" +
                "id: 2\tname: Name 2\tphoneNumber: 123456\n" +
                "id: 3\tname: Name 3\tphoneNumber: 1234567\n"
        assertEquals(expectedContacts, addressBook.printContacts())

    }
}