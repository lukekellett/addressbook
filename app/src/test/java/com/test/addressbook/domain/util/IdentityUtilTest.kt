package com.test.addressbook.domain.util

import com.test.addressbook.com.test.addressbook.domain.util.IdentityUtil
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class IdentityUtilTest {

    @Before
    fun setup() {
        IdentityUtil.reset()
    }

    @Test
    fun getNextId_idsIncrementSequentially() {
        Assert.assertEquals(1, IdentityUtil.getNextId())
        Assert.assertEquals(2, IdentityUtil.getNextId())
        Assert.assertEquals(3, IdentityUtil.getNextId())
        IdentityUtil.reset()
        Assert.assertEquals(1, IdentityUtil.getNextId())
        Assert.assertEquals(2, IdentityUtil.getNextId())
        Assert.assertEquals(3, IdentityUtil.getNextId())
    }

}